<?php
	function crearbd(){
		$crear_tabla = 'CREATE TABLE IF NOT EXISTS contactos(
						id INTEGER PRIMARY KEY AUTOINCREMENT,
						nombre VARCHAR(40), 
						apellidos VARCHAR(60), 
						telefono VARCHAR(10),
						correo VARCHAR(50)
						)';	
	
		$nombre_fichero = 'agenda.sqlite';
		if (!file_exists($nombre_fichero)) {
	
			try{
				// 1.- Crear base de datos (sqlite solo la ruta, mysql, ip, usuario y password)
				$conn = new PDO('sqlite:agenda.sqlite'); //crea conexion y crea la bd si no existe
				$conn -> exec($crear_tabla); //crear tabla
			}
			catch(PDOException $e ){
				echo $e->getMessage();
			}
			$conn = null;
		}
	}
	
	function generarformulario($accion, $contacto = ""){		
		echo '	<form action="'. $accion .'" method="POST">
				<table>
					<tr>
						<td><label for="nombre">Nombre:</label></td>';
		echo	'		<td><input type="text" id="nombre" name="nombre"';
		
		if($accion =='editar2.php'){
			echo "value ='" .$contacto['nombre']."'";
		}
		echo    '/></td>
					</tr>
					<tr>
						<td><label for="apellidos">apellidos:</label></td>';
		echo	'		<td><input type="text" id="apellidos" name="apellidos"';
		if($accion =='editar2.php'){
			echo "value ='" .$contacto['apellidos']."'";
		}
		echo    '/></td>
					</tr>
					<tr>
						<td><label for="email">email:</label></td>';
		echo	'		<td><input type="text" id="email" name="email"';
		if($accion =='editar2.php'){
			echo "value ='" .$contacto['correo']."'";
		}
		echo    '/></td>
					</tr>
					<tr>
						<td><label for="telefono">telefono:</label></td>';
		echo	'		<td><input type="text" id="telefono" name="telefono"';
		if($accion =='editar2.php'){
			echo "value ='" .$contacto['telefono']."'";
		}
		echo    '/></td>
					</tr>
					<tr><input type="submit" value="Enviar" /></tr>
				</table>
			</form>';
	}
	
	function contectar($conn){
		try{
			$conn = new PDO('sqlite:agenda.sqlite'); 
			return true;			
		}catch(PDOException $e ){
			echo $e -> getMessage();
			return false;
		}
	}
	
	function listar(){	
		try{
			$conn = new PDO('sqlite:agenda.sqlite');
			$consulta = "SELECT * FROM contactos";
			$resultado = $conn -> query($consulta);
			
			echo "<table class='contactos'>
					<th>
						<tr><td>Nombre</td><td>Apellidos</td><td>Telefono</td><td>Email</td></tr>
					</th>";
			foreach ($resultado as $contacto) {
				echo "<tr><td>", $contacto['nombre'],"</td>";
				echo "<td>", $contacto['apellidos'],"</td>";
				echo "<td>", $contacto['telefono'],"</td>";
				echo "<td>", $contacto['correo'],"</td></tr>";
			}
			echo "</table>";
			$conn = null;
		}catch(PDOException $e ){
			echo $e -> getMessage();
		}	
	}
	function preparareliminar(){	
		try{
			$conn = new PDO('sqlite:agenda.sqlite');
			$consulta = "SELECT * FROM contactos";
			$resultado = $conn -> query($consulta);
			
			echo "<form action='borrar.php' method='POST'><table class='contactos'>
					<th>
						<tr><td>borrar</td><td>Nombre</td><td>Apellidos</td><td>Telefono</td><td>Email</td></tr>
					</th>";
			foreach ($resultado as $contacto) {
				echo "<tr><td><input type='checkbox' value='".$contacto['id']."' name='del[]'/></td>";
				echo "<td>", $contacto['nombre'],"</td>";
				echo "<td>", $contacto['apellidos'],"</td>";
				echo "<td>", $contacto['telefono'],"</td>";
				echo "<td>", $contacto['correo'],"</td></tr>";
			}
			echo "</table><input type='submit' value='borrar'/></form>";
			$conn = null;
		}catch(PDOException $e ){
			echo $e -> getMessage();
		}	
	}
	
	function preparareditar(){	
		try{
			$conn = new PDO('sqlite:agenda.sqlite');
			$consulta = "SELECT * FROM contactos";
			$resultado = $conn -> query($consulta);
			
			echo "<form method='GET' action='editar2.php'><table class='contactos'>
					<th>
						<tr><td>editar</td><td>Nombre</td><td>Apellidos</td><td>Telefono</td><td>Email</td></tr>
					</th>";
			foreach ($resultado as $contacto) {
				echo "<tr><td><input type='radio' id=".$contacto['id']." value=".$contacto['id']." name='mod'</td>";
				echo "<td>", $contacto['nombre'],"</td>";
				echo "<td>", $contacto['apellidos'],"</td>";
				echo "<td>", $contacto['telefono'],"</td>";
				echo "<td>", $contacto['correo'],"</td></tr>";
			}
			echo "</table><input type='submit' value='editar'/></form>";
			$conn = null;
		}catch(PDOException $e ){
			echo $e -> getMessage();
		}	
	}
	
	function insertar($nombre, $apellidos, $telf, $email){
		try{
			$conn = new PDO('sqlite:agenda.sqlite');
			$consulta = "INSERT INTO contactos (nombre, apellidos, telefono, correo)
				VALUES ('" .$nombre ."', '" . $apellidos . "', '"  . $telf . "', '" . $email . "')";
			$conn -> exec($consulta);
			
			$conn = null;
			header("Location: index.php");
		}catch(PDOException $e ){
			echo $e -> getMessage();
		}		
	}
	
	function guardarCambios($id, $nombre, $apellidos, $telf, $email){
		try{
			$conn = new PDO('sqlite:agenda.sqlite');
			$consulta = "UPDATE contactos SET (nombre ='" .$nombre ."', apellidos = '" . $apellidos . "', telefono = '"  . $telf . "', correo = '" . $email . "')
						WHERE id = '" . $id . "'";
			$conn -> exec($consulta);
			
			$conn = null;
			header("Location: index.php");
		}catch(PDOException $e ){
			echo $e -> getMessage();
		}
	}
	function editar(){
		if ($_REQUEST){
			$nom = $_POST['nombre'];
			$ape = $_POST['apellidos'];
			$email = $_POST['email'];
			$telf = $_POST['telefono'];
		}
	}
	function sesiones(){
		session_start();
		if (isset($_SESSION["user"], $_SESSION["hash"])){			
	    	return true;
		}else{
	    	return false;
		}
	}
?>