<?php
	include('funciones.php');
	
	if (!sesiones()){
		header("Location:login.php");
	};
	
	include('header.html');
?>

	<div class="contenido">
		<?php
			generarformulario("insertar.php");		
		?>
	</div>
<?php
	if(isset($_POST['nombre'], $_POST['apellidos'], $_POST['email'], $_POST['telefono'])){
		$nom = $_POST['nombre'];
		$ape = $_POST['apellidos'];
		$email = $_POST['email'];
		$telf = $_POST['telefono'];
	
		insertar($nom, $ape, $telf, $email);
	}
	
	include('footer.html');
?>