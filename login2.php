<?php
	include 'header.php';
	include 'password.php';
	
	/*$user = "usuario";
	$pass = "usuario";*/
	$user = "usuario";
	$correct_hash = 'pbkdf2_sha256$12000$hXG8XATVtSM=$OkEmRGauUwHinFiGNo/UQRAcTStGuK0ocK45gwfAbsU=';
	
	session_start();
	
	if (isset($_POST['user'], $_POST['pass'])){
		$usuario = $_POST['user'];
		$password = $_POST['pass'];
		if ($user == $usuario){
			if (validate_password($password, $correct_hash)){
				$_SESSION['user'] = $usuario;
				$password = create_hash($password);
				$_SESSION['hash'] = $password;
				header("Location:index.php");
			}else{
				echo "<p>La contraseña introducida es incorrecta. <a href='login.php'>Volver al login</a>.<p/>";
			}
		}else{
			echo "<p>El nombre del usuario es incorrecto. <a href='login.php'>Volver al login</a>.<p/>";
		}
		session_regenerate_id(); //regenerar un id al hacer algun cambio
	}
	include 'footer.php';
?>